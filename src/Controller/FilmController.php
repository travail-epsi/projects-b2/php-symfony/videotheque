<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmType;
use App\Manager\FilmManager;
use App\Manager\NotationManager;
use App\Manager\RoleManager;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    private function getManager(): FilmManager
    {
        return new FilmManager($this->getDoctrine());
    }

    /**
     * @Route("/film/index/{page}", defaults={"page" = 1}, name="film_index")
     * @param $page
     * @return Response
     */
    public function index($page): Response
    {
        if ($page < 1) {
            $page = 1;
        }
        // On utilise un paramètre pour le nombre de page
        $nbPerPage = $this->getParameter('nb_per_page');
        // On récupère notre objet Paginator
        $films = $this->getManager()->loadAllFilms($page, $nbPerPage);
        // On calcule le nombre total de pages grâce au count($films) qui retourne le nombre total de films
        $nbPages = ceil(count($films) / $nbPerPage);
        // On donne toutes les informations nécessaires à la vue
        return $this->render('film/index.html.twig', [
            'arrayFilms' => $films,
            'nbPages' => $nbPages,
            'page' => $page
        ]);
    }

    /**
     * @Route ("film/edit/{id}", name="film_edit")
     * @param Request $request
     * @param $id
     * @return Response
     * @throws OptimisticLockException
     */
    public function edit(Request $request, $id): Response
    {
        $filmManager = $this->getManager();
        if (!$film = $filmManager->loadFilm($id)) {
            throw new NotFoundHttpException("Le film n'existe pas");
        }
        $form = $this->createForm(FilmType::class, $film);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filmManager->saveFilm($film);
            return $this->redirectToRoute('film_index');
        }


        $roleManager = new RoleManager($this->getDoctrine());
        $roles = $roleManager->loadRolesFromFilm($id);

        $notationManager = new NotationManager($this->getDoctrine());
        $notes = $notationManager->loadAllByFilm($id);
        return $this->render('film/edit.html.twig', [
            'film' => $film,
            'form' => $form->createView(),
            'roles' => $roles,
            'notes' => $notes
        ]);
    }

    /**
     * @Route("film/add", name="film_add")
     * @param Request $request
     * @return Response
     * @throws OptimisticLockException
     */
    public function add(Request $request): Response
    {
        // Création d'un nouveau film
        $film = new Film();
        // Création du modèle du formulaire
        $form = $this->createForm(FilmType::class, $film);
        // Attachement du modèle à l'objet "request"
        $form->handleRequest($request);
        // Si l'utilisateur soumet le formulaire et que le formulaire est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Obtention du manager
            $manager = $this->getManager();
            // Validation de l'entité
            $manager->saveFilm($film);
            return $this->redirectToRoute('film_index');
        }
        //J'ai décidé de faire une page Twig à part de celle déjà existante pour que ça soit plus simple
        return $this->render('film/add.html.twig', [
            'form' => $form->createView(),
            'film' => $film
        ]);
    }

}