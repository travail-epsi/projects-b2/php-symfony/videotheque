<?php

namespace App\Controller;

use App\Produits\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    private function getProduits()
    {
        $produits = array();
        $produits[] = new Produit(1, "Table", 2, 100);
        $produits[] = new Produit(2, "Chaise", 14, 40);
        $produits[] = new Produit(3, "Table basse", 6, 85);
        return $produits;
    }

    /**
     * @Route("/produit", name="produit_index")
     */
    public function index()
    {
        $produits = $this->getProduits();
        return $this->render(
            'produit/index.html.twig', [
            'listeProduits' => $produits
        ]);
    }
}