<?php

namespace App\Controller;

use App\Manager\FilmManager;
use App\Manager\NotationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotationController extends AbstractController
{
    private function getManager(): NotationManager
    {
        return new NotationManager($this->getDoctrine());
    }

    /**
     * @Route("/notation", name="notation")
     */
    public function index(): Response
    {
        $notes = $this->getManager()->loadAllNotation();
        return $this->render('notation/index.html.twig', [
            'notes' => $notes
        ]);
    }

    /**
     * @Route("/notation/{idFilm}", name="film_notation")
     * @param $idFilm
     * @return Response
     */
    public function byFilm($idFilm): Response
    {
        $allNotations = $this->getManager()->loadAllNotation();
        $infoNote = null;
        foreach ($allNotations as $notation) {
            if ($notation['id'] == $idFilm) {
                $infoNote = $notation;
                break;
            }
        }
        $notes = $this->getManager()->loadAllByFilm($idFilm);

        return $this->render('notation/byFilm.html.twig', [
            'infoNote' => $infoNote,
            'notes' => $notes

        ]);
    }

//AUTRE VERSION DE LA ROUTE (FONCTIONNE AUSSI)
//    /**
//     * @Route("/notation/{idfilm}", defaults={"id" = 1}, name="film_notation")
//     */
//    public function byFilm($idfilm): Response
//    {
//        $manager = new NotationManager($this->getDoctrine());
//        $notes = $manager->loadAllByFilm($idfilm);
//
//        $filmManager = new FilmManager($this->getDoctrine());
//        $film = $filmManager->loadFilm($idfilm);
//        return $this->render('notation/byfilm.html.twig', [
//            'notes' => $notes,
//            'film' => $film
//        ]);
//    }

}
