<?php

namespace App\Controller;

use App\Entity\Artiste;
use App\Form\ArtisteType;
use App\Manager\ArtisteManager;
use App\Manager\RoleManager;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ArtisteController extends AbstractController
{
    private function getManager(): ArtisteManager
    {
        return new ArtisteManager($this->getDoctrine());
    }

    /**
     * @Route("/artiste/index/{page}", defaults={"page" = 1}, name="artiste_index")
     * @param $page
     * @return Response
     */
    public function index($page): Response
    {
        if ($page < 1) {
            $page = 1;
        }
        $nbPerPage = $this->getParameter('nb_per_page');
        $artiste = $this->getManager()->loadAllArtistes($page, $nbPerPage);
        $nbPages = ceil(count($artiste) / $nbPerPage);
        return $this->render('artiste/index.html.twig', [
            'arrayArtiste' => $artiste,
            'nbPages' => $nbPages,
            'page' => $page
        ]);
    }

    /**
     * @Route ("artiste/info/{id}", name="artiste_info")
     */
    public function info($id): Response
    {
        $artisteManager = $this->getManager();
        $artiste = $artisteManager->loadArtiste($id);

        $roleManager = new RoleManager($this->getDoctrine());
        $roles = $roleManager->loadRolesFromArtiste($id);

        return $this->render('artiste/info.html.twig', [
            'artiste' => $artiste,
            'roles' => $roles
        ]);
    }

    /**
     * @Route ("artiste/edit/{id}", name="artiste_edit")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function edit(Request $request, $id): Response
    {
        $artisteManager = $this->getManager();
        if(!$artiste = $artisteManager->loadArtiste($id)){
            throw new NotFoundHttpException("L'artiste n'existe pas");
        }
        $form = $this->createForm(ArtisteType::class, $artiste);

        $form->handleRequest($request);
        // Si l'utilisateur soumet le formulaire et que le formulaire est valide
        if ($form->isSubmitted() && $form->isValid())
        {
            // Validation de l'entité
            $artisteManager->saveArtiste($artiste);
            return $this->redirectToRoute('artiste_index');
        }

        return $this->render('artiste/edit.html.twig', [
            'artiste' => $artiste,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("artiste/add", name="artiste_add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        // Création d'un nouveau artiste
        $artiste = new Artiste();
        // Création du modèle du formulaire
        $form = $this->createForm(ArtisteType::class, $artiste);
        // Attachement du modèle à l'objet "request"
        $form->handleRequest($request);
        // Si l'utilisateur soumet le formulaire et que le formulaire est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Obtention du manager
            $manager = $this->getManager();
            // Validation de l'entité
            $manager->saveArtiste($artiste);
            return $this->redirectToRoute('artiste_index');
        }
        //J'ai décidé de faire une page Twig à part de celle déjà existante pour que ça soit plus simple
        return $this->render('artiste/add.html.twig', [
            'form' => $form->createView(),
            'artiste' => $artiste
        ]);
    }

}
