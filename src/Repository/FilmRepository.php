<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Film::class);
    }


    /**
     * @param $page int Numéro de page demandé
     * @param $nbPerPage int Nombre de pages totales
     * @return Paginator Paginator
     */
    public function findAllOrderedByTitle($page, $nbPerPage): Paginator
    {
        $query = $this->getEntityManager()
            ->createQuery('SELECT f
                FROM App:Film f
                ORDER BY f.titre ASC');
        $query
            // On définit le film à partir duquel commence la liste
            ->setFirstResult(($page - 1) * $nbPerPage)
            // Ainsi que le nombre de films à afficher sur une page
            ->setMaxResults($nbPerPage);
        // Enfin, on retourne l'objet Paginator correspondant à la requête construite
        return new Paginator($query, true);
    }


    // /**
    //  * @return Film[] Returns an array of Film objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByTitle($title): ?Film
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.titre = :val')
            ->setParameter('val', $title)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }


}
