<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notation
 *
 * @ORM\Table(name="notation", indexes={@ORM\Index(name="idFilm", columns={"idFilm", "email"}), @ORM\Index(name="email", columns={"email"}), @ORM\Index(name="IDX_268BC95F1B9E0BC", columns={"idFilm"})})
 * @ORM\Entity(repositoryClass="App\Repository\NotationRepository")
 */
class Notation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="note", type="integer", nullable=false)
     */
    private $note;

    /**
     * @var \Film
     *
     * @ORM\ManyToOne(targetEntity="Film")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFilm", referencedColumnName="id")
     * })
     */
    private $idfilm;

    /**
     * @var \Internaute
     *
     * @ORM\ManyToOne(targetEntity="Internaute")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="email", referencedColumnName="email")
     * })
     */
    private $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIdfilm(): ?Film
    {
        return $this->idfilm;
    }

    public function setIdfilm(?Film $idfilm): self
    {
        $this->idfilm = $idfilm;

        return $this;
    }

    public function getEmail(): ?Internaute
    {
        return $this->email;
    }

    public function setEmail(?Internaute $email): self
    {
        $this->email = $email;

        return $this;
    }


}
