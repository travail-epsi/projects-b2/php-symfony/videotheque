<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Film
 *
 * @ORM\Table(name="film", indexes={@ORM\Index(name="codePays", columns={"codePays"}), @ORM\Index(name="idRealisateur", columns={"idRealisateur"}), @ORM\Index(name="genre", columns={"idGenre"})})
 * @ORM\Entity(repositoryClass="App\Repository\FilmRepository")
 */
class Film
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=50, nullable=false)
     */
    private $titre;

    /**
     * @var int
     *
     * @ORM\Column(name="annee", type="integer", nullable=false)
     */
    private $annee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="resume", type="text", length=65535, nullable=true)
     */
    private $resume;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codePays", type="string", length=4, nullable=true)
     */
    private $codepays;

    /**
     * @var \Artiste
     *
     * @ORM\ManyToOne(targetEntity="Artiste")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idRealisateur", referencedColumnName="id")
     * })
     */
    private $idrealisateur;

    /**
     * @var \Genre
     *
     * @ORM\ManyToOne(targetEntity="Genre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idGenre", referencedColumnName="id")
     * })
     */
    private $idgenre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(?string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getCodepays(): ?string
    {
        return $this->codepays;
    }

    public function setCodepays(?string $codepays): self
    {
        $this->codepays = $codepays;

        return $this;
    }

    public function getIdrealisateur(): ?Artiste
    {
        return $this->idrealisateur;
    }

    public function setIdrealisateur(?Artiste $idrealisateur): self
    {
        $this->idrealisateur = $idrealisateur;

        return $this;
    }

    public function getIdgenre(): ?Genre
    {
        return $this->idgenre;
    }

    public function setIdgenre(?Genre $idgenre): self
    {
        $this->idgenre = $idgenre;

        return $this;
    }


}
