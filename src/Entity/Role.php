<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table(name="role", indexes={@ORM\Index(name="idFilm", columns={"idFilm", "idActeur"}), @ORM\Index(name="idActeur", columns={"idActeur"}), @ORM\Index(name="IDX_57698A6AF1B9E0BC", columns={"idFilm"})})
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="role", type="string", length=30, nullable=true)
     */
    private $role;

    /**
     * @var \Film
     *
     * @ORM\ManyToOne(targetEntity="Film")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFilm", referencedColumnName="id")
     * })
     */
    private $idfilm;

    /**
     * @var \Artiste
     *
     * @ORM\ManyToOne(targetEntity="Artiste")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idActeur", referencedColumnName="id")
     * })
     */
    private $idacteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getIdfilm(): ?Film
    {
        return $this->idfilm;
    }

    public function setIdfilm(?Film $idfilm): self
    {
        $this->idfilm = $idfilm;

        return $this;
    }

    public function getIdacteur(): ?Artiste
    {
        return $this->idacteur;
    }

    public function setIdacteur(?Artiste $idacteur): self
    {
        $this->idacteur = $idacteur;

        return $this;
    }


}
