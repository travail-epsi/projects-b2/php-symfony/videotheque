<?php


namespace App\Manager;


use App\Entity\Role;
use Doctrine\Persistence\ManagerRegistry;

class RoleManager
{
    /* L'objet central de Doctrine : Manager Registry */
    protected $managerRegistry;
    /* Le référentiel lié à l'entité Role */
    protected $repository;
    /**
     * RoleManager constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        /* Le contructeur nous permet de conserver le Manager Registry ... */
        $this->managerRegistry = $managerRegistry;
        /* ... et de créer le référentiel lié à l'entité Role */
        $this->repository = $managerRegistry->getRepository(Role::class);
    }
    /**
     * Load all Role entity for specified id film
     *
     * @return Role[] | null
     */
    public function loadRolesFromFilm($idfilm)
    {
        return $this->repository->findByFilm($idfilm);
    }

    /**
     * Load all Role entity for specified id artiste
     *
     * @return Role[] | null
     */
    public function loadRolesFromArtiste($idArtiste)
    {
        return $this->repository->findByArtiste($idArtiste);
    }
}