<?php

namespace App\Manager;

use App\Entity\Film;
use Doctrine\Persistence\ManagerRegistry;

class FilmManager
{
    /* L'objet central de Doctrine : Manager Registry */
    protected $managerRegistry;
    /* Le référentiel lié à l'entité Film */
    protected $repository;

    /**
     * FilmManager constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        /* Le contructeur nous permet de conserver le Manager Registry ... */
        $this->managerRegistry = $managerRegistry;
        /* ... et de créer le référentiel lié à l'entité Film */
        $this->repository = $managerRegistry->getRepository(Film::class);
    }

    /**
     * Load all Film entity
     *
     * @param $page
     * @param $nbPerPage
     * @return Film[]
     */
    public function loadAllFilms($page, $nbPerPage)
    {
        return $this->repository->findAllOrderedByTitle($page, $nbPerPage);
    }

    /**
     * Load Film entity
     *
     * @param Integer $filmId
     * @return Film
     */
    public function loadFilm($filmId)
    {
        return $this->repository->find($filmId);
    }

    public function getByTitle($title)
    {
        return $this->repository->findOneByTitle($title);
    }

    /**
     * Save Film entity
     *
     * @param Film $film
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFilm(Film $film)
    {
        $this->managerRegistry->getManager()->persist($film);
        $this->managerRegistry->getManager()->flush();
    }

    /**
     * Remove Film entity
     *
     * @param Integer $filmId
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeFilm(Film $film)
    {
        $this->managerRegistry->getManager()->remove($film);
        $this->managerRegistry->getManager()->flush();
    }


}