<?php


namespace App\Manager;

use App\Entity\Notation;
use Doctrine\Persistence\ManagerRegistry;

class NotationManager
{
    /* L'objet central de Doctrine : Manager Registry */
    protected $managerRegistry;
    /* Le référentiel lié à l'entité Film */
    protected $repository;

    /**
     * FilmManager constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        /* Le contructeur nous permet de conserver le Manager Registry ... */
        $this->managerRegistry = $managerRegistry;
        /* ... et de créer le référentiel lié à l'entité Film */
        $this->repository = $managerRegistry->getRepository(Notation::class);
    }


    /** Une partie du code a été omis */
    /**
     * @return Notation[]|array
     */
    public function loadAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @return Notation[]|array
     */
    public function loadAllByFilm($idfilm)
    {
        $qb = $this->repository->createQueryBuilder('n');
        $qb->andWhere('n.idfilm = :idfilm')
            ->setParameter(':idfilm', $idfilm);
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function loadAllNotation()
    {
        return $this->repository->loadAllGroupBy();
    }
}
