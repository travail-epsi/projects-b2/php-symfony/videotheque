<?php


namespace App\Manager;


use App\Entity\Artiste;
use Doctrine\Persistence\ManagerRegistry;

class ArtisteManager
{

    /* L'objet central de Doctrine : Manager Registry */
    protected $managerRegistry;
    /* Le référentiel lié à l'entité Artiste */
    protected $repository;

    /**
     * ArtisteManager constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        /* Le contructeur nous permet de conserver le Manager Registry ... */
        $this->managerRegistry = $managerRegistry;
        /* ... et de créer le référentiel lié à l'entité Artiste */
        $this->repository = $managerRegistry->getRepository(Artiste::class);
    }

    /**
     * Load all Artiste entity
     *
     * @param $page
     * @param $nbPerPage
     * @return Artiste[]
     */
    public function loadAllArtistes($page, $nbPerPage)
    {
        return $this->repository->findAllOrderedByName($page, $nbPerPage);
    }

    /**
     * Load Artiste entity
     *
     * @param Integer $artisteId
     * @return Artiste
     */
    public function loadArtiste(int $artisteId)
    {
        return $this->repository->find($artisteId);
    }

    public function getByTitle($title)
    {
        return $this->repository->findOneByTitle($title);
    }

    /**
     * Save Artiste entity
     *
     * @param Artiste $artiste
     */
    public function saveArtiste(Artiste $artiste)
    {
        $this->managerRegistry->getManager()->persist($artiste);
        $this->managerRegistry->getManager()->flush();
    }

    /**
     * Remove Artiste entity
     *
     * @param Artiste $artiste
     */
    public function removeArtiste(Artiste $artiste)
    {
        $this->managerRegistry->getManager()->remove($artiste);
        $this->managerRegistry->getManager()->flush();
    }


}