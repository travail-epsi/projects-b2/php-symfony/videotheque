<?php

namespace App\Form;

use App\Entity\Artiste;
use App\Entity\Film;
use App\Entity\Genre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Ajout des champs "classiques"
        $builder
            ->add('titre', TextType::class , ['label' => 'Titre'])
            ->add('annee', NumberType::class, ['label' => 'Année'])
            ->add('resume', TextareaType::class, ['label' => 'Résumé']);
        // Ajout des champs liés à une table
        $builder->add('idgenre', EntityType::class, [
            'class' => Genre::class,
            'required' =>true,
            'label' => "Genre",
            'choice_label' => 'nom',
        ]);
        $builder->add('idrealisateur', EntityType::class, [
            'class' => Artiste::class,
            'required' =>true,
            'label' => "Réalisateur",
            'choice_label' => 'nom',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Film::class,
        ]);
    }
}
